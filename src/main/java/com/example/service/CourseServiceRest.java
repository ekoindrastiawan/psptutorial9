package com.example.service;

import com.example.dao.CourseDAO;
import com.example.model.CourseModel;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@Primary
public class CourseServiceRest implements CourseService {

    @Autowired
    CourseDAO courseDAO;

    @Override
    public CourseModel selectCourse(String idCourse) {
        log.info("REST - select course with id_course {}", idCourse);
        return courseDAO.selectCourse(idCourse);
    }

    @Override
    public List<CourseModel> selectAllCourse() {
        log.info("REST - select all courses");
        return courseDAO.selectAllCourses();
    }

    @Override
    public void addCourse(CourseModel course) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteCourse(String idCourse) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateCourse(CourseModel course) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
